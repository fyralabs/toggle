# Getting Started

The easiest way to get started with contributing to Toggle is to utilize [GNOME Builder](flathub.org/apps/org.gnome.Builder).

You may need to run `git submodule update --init --recursive` in order to pull the type declaration submodules. 

If Builder doesn't do it for you automatically, you need to enable the following Flatpak remotes:

- [gnome-nightly](https://wiki.gnome.org/Apps/Nightly)
- [flathub](flatpak.org/setup)

And install the following packages:

- `org.gnome.Platform//master`
- `org.gnome.Sdk//master`
- `org.freedesktop.Sdk.Extension.node18//23.08`
- `org.freedesktop.Sdk.Extension.typescript//23.08`

You can now build and run development builds of Toggle directly from Builder with ease. However, if you need to, you can build Toggle from source manually like so:

1. Install the previously listed packages from the required remotes
2. Install `org.flatpak.Builder` from Flathub
3. In the project root, run `flatpak run --filesystem=$(pwd) org.flatpak.Builder --force-clean build-dir build-aux/io.gitlab.orowith2os.Toggle.Devel.json`
 
You may also want to install and run the package on the user Flatpak installations by adding the following flags: 

- `--user`
- `--install`

Afterwards, you can run the built app (assuming you installed it) with `flatpak run io.gitlab.orowith2os.Toggle.Devel` or by clicking the development app icon in your application drawer.

# Contributing to the Project

Thank you for contributing to Toggle! Please note the following before you contribute:

## Code of Conduct (CoC)

Before contributing, please familiarize yourself with the Contributor Covenant, located [here](./CODE_OF_CONDUCT.md).

## Working on Tasks

Collaboration is essential to making great software. Before starting a task:

1. Check if the task you'd like to complete is listed as a To-Do item in the [README.md](./README.md)
2. If your task is not listed as a To-Do item, [join us on our Matrix channel](https://matrix.to/#/#toggle-application:fedora.im) and discuss your task proposal with us!
3. Once you've gotten the go-ahead, please open a new task on the [Kanban, or "Issue Boards" section](https://gitlab.com/OroWith2Os/toggle/-/boards) so we know what you're working on.
  Mark it as "WIP", assign it to yourself, and optionally, add a body.

  ![Open an issue in the WIP column](./docs/create-issue.png)
  ![Assign it to yourself in the right panel](./docs/assign-issue.png)
  
4. Open a PR, and drag and drop your task into the "PR Submitted" column. Flag us down in the channel with a link to your PR for reviews.
5. Get your PR merged.

Congratulations! You just collaborated to Toggle.
This process ensures no one produces work uselessly or accidentally works on something someone else is already working on.