using Gtk 4.0;
using Adw 1;

template $ToggleAppearancePreferencePage : Adw.PreferencesPage {
  Adw.PreferencesGroup {
    title: _("Themes");

    [header-suffix]
    Button open_themes_directory {
      halign: center;
      valign: center;
      icon-name: "folder-open-symbolic";
      tooltip-text: "Open the user themes directory";
      styles [
        "flat",
      ]
    }

    Adw.ComboRow cursor_themes {
      title: _("Cursor");
    }

    Adw.ComboRow icon_themes {
      title: _("Icons");
    }
  }

  Adw.PreferencesGroup {
    title: _("Fonts");
    
    Adw.ActionRow {
      title: _("Interface");
      activatable-widget: interface_font;
      
      FontDialogButton interface_font {
        valign: center;
        use-font: true;
      }
    }
    
    Adw.ActionRow {
      title: _("Documents");
      activatable-widget: documents_font;
      
      FontDialogButton documents_font {
        valign: center;
        use-font: true;
      }
    }
    
    Adw.ActionRow {
      title: _("Monospace");
      activatable-widget: monospace_font;
      
      FontDialogButton monospace_font {
        valign: center;
        use-font: true;
      }
    }
    
    Adw.ComboRow font_hinting {
      title: _("Hinting");
      model: font_hinting_list;
    }
    
    Adw.ComboRow font_antialiasing {
      title: _("Anti-Aliasing");
      model: font_antialiasing_list;
    }
    
    Adw.SpinRow font_scaling_factor {
      title: _("Scaling Factor");
      digits: 2;

      adjustment: Adjustment {
        lower: 0.5;
        upper: 3;
        step-increment: 0.25;
        value: 1;
      };
    }
  }
}

StringList font_hinting_list {
  strings [
    _("Full"),
    _("Medium"),
    _("Slight"),
    _("None")
  ]
}

StringList font_antialiasing_list {
  strings [
    _("Subpixel"),
    _("Standard"),
    _("None")
  ]
}
