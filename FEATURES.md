## Features from Tweaks

Most of these have been noted under [GNOME Design Team Initiatives #52](https://gitlab.gnome.org/Teams/Design/initiatives/-/issues/52), but more details will also be linked here.

Note that this file may become outdated as time goes on; take all information here with a grain of salt.

### General

- Suspend when laptop lid is closed - should be moved to Settings, needs investigation on how to properly implement in Toggle for now.
- Over-amplification is already in Settings; as of the time of writing, this is under Accessibility->Hearing->Overamplification, so we can remove it.

### Appearance, Theming, Fonts

We should try and get rid of Legacy Application and the Shell theming, in favor of Gradience: [Gradience#805](https://github.com/GradienceTeam/Gradience/issues/805)

For others from the Appearance menu, specifically the following, they should somehow be exposed nicely in Toggle:

- Cursor
- Icons
- Sound (Settings has this already, so can probably safely be removed; research to see if it uses a freedesktop spec and is still applicable)

The Background selection is already in Settings, and the Lock Screen option is nonfunctional. At most, we should keep the Adjustment option, but that should be moved to Settings too if possible. For the time being, don't keep it, since it's generally broken and a pain to deal with.

Fonts also fall into this category, so we should expose them if possible. It would be nice if this could also be moved to Settings, but it's probably not happening, and it probably shouldn't, unless Settings gets an "Advanced" submenu or the like. Scaling Factor, at least, is WIP: [gnome-control-center#1648](https://gitlab.gnome.org/GNOME/gnome-control-center/-/issues/1648)

### Keyboard & Mouse

- The Emacs Input option can be removed, apparently it's gone and nonfunctional in GTK4
- Overview Shortcut - see [gnome-control-center#2605](https://gitlab.gnome.org/GNOME/gnome-control-center/-/issues/2605) and [gnome-control-center#2183](https://gitlab.gnome.org/GNOME/gnome-control-center/-/issues/2183), Toggle will implement it for now.
- Additional Layout Options should ideally be put into Settings; there's already a View and Customize Keyboard Shortcuts menu for that, so probably fine to remove when support is added.

Mouse:
- Acceleration Profile can be removed, it's in Settings
- Pointer Location can be removed, it's in Settings already.
- Middle Click Paste - Settings?

Touchpad:
- Disable While Typing should go to Settings
- Mouse Click Emulation should be moved to Settings

### Autostart

The [Background portal](https://flatpak.github.io/xdg-desktop-portal/#gdbus-org.freedesktop.portal.Background) should handle most everything for us; additionally, for non-Flatpak applications, most of them provide `.desktop` files and an icon, so Settings can manage them just fine. 

For applications that want to ask to run with a specific command, like if they don't want a GUI to show in the case of a backup tool, this is in scope of the Background portal; applications can specify a custom command to be ran on startup. 

It could be improved by allowing the Background portal to be sent a custom desktop file or name of one, and that gets handed off - it would make separating a daemon and GUI frontend easier, and allow applications like Settings to have a distinction between the two.

Toggle could manage custom desktop files itself, as they probably wouldn't be suitable inside of Settings given most custom services don't have icons, but should we manage this? It can be considered. 

In any case, we should be trying to move this functionality to Settings as much as possible. It's mostly doable. Several applications, including Valent, already utilize this, so there's at least a bit of demand for it. For the time being, maybe Toggle could be the application to demo it and show how part of it could work.

### Top Bar

Can be removed, all of the options available are in Settings.

### Window Titlebars

The Titlebar Actions menu should be kept.

The Titlebar Buttons menu needs a bit of work to reliably put into UI form, but should be kept.

### Windows

- Look at removing the Attach Modal Dialogs option, it causes issues with the shell overview close action and results in a mistaken workflow with the dialogs.
- Keep Center New Windows until it's the default and the rest of [mutter#2123](https://gitlab.gnome.org/GNOME/mutter/-/issues/2123) is resolved
- Resize with Secondary Click should be kept
- Window action Key - should it be removed, or set to the "modifier" key for GNOME? Assuming such a concept of a key exists on GNOME in the first place. An issue needs filing, and the internals need research.
- Window Focus MAY be kept, but appears to be broken; depends on outcome of [gsettings-desktop-schemas#49](https://gitlab.gnome.org/GNOME/gsettings-desktop-schemas/-/issues/49)
